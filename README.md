# nodejs web app 

https://nodejs.org/api/synopsis.html

https://nodejs.org/en/docs/guides/nodejs-docker-webapp/

https://github.com/cmuth001/Dockerizing-a-NodeJS-web-app


# Dockerfile

```bash
# Dockerfile 
docker build -t node-web-app:01 .
docker run -it -d -p 8080:8080 --name node-web-app node-web-app:01
```


If we want to build nodejs without docker  
```bash
npm init 
npm install
npm server.js
```
