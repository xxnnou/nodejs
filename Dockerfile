FROM node:18.12.1


# Create app directory
WORKDIR "/app/"


# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
ADD package*.json /app/
RUN npm install

# If you are building your code for production
# RUN npm ci --only=production


# Bundle app source
COPY . "/app/"


EXPOSE 8080


CMD [ "node", "server.js" ]
